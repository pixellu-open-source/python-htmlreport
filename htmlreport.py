__author__ = "Alex Prykhodko (Pixellu LLC)"

import unittest
from copy import deepcopy
import re

INDEX_NOT_FOUND = -1


#==============================================================================
# Basic HTML template definitions
#==============================================================================

class _HTMLReportTemplate(object):

    BOOTSTRAP_URL = "http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"

    @classmethod
    def table(cls, head_contents, body_contents):
        return \
            '''<table class="table table-hover">
                <thead>
                    %s
                </thead>
                <tbody>
                    %s
                </tbody>
                </table>
            ''' % (head_contents, body_contents)

    @classmethod
    def bootstrap_stylesheet(cls):
        return '''<link rel="stylesheet" href="%s">''' % cls.BOOTSTRAP_URL

    @classmethod
    def html(cls, head_contents, body_contents):
        html = \
            '''<html>
            <head>
                %s
            </head>
            <body>
                %s
            </body>
            </html>
            ''' % (head_contents, body_contents)
        return html

    @classmethod
    def report_heading(cls, title, errors_count, warnings_count):
        heading = '<h2>%s\n' % title
        if errors_count > 0:
            heading += '<span class="label label-danger">%d Error(s)</span>\n' % errors_count
        if warnings_count > 0:
            heading += '<span class="label label-warning">%d Warning(s)</span>\n    ' % warnings_count
        heading += '</h2>'
        return heading

    @classmethod
    def bootstrap_info_alert(cls, alert_contents):
        return '''<div class="alert alert-info">%s</div>''' % (alert_contents)

    @classmethod
    def bootstrap_icon(cls, icon_class):
        return '<span class="glyphicon %s"></span>' % icon_class

    @classmethod
    def bootstrap_label(cls, label_class, label_content):
        return '<span class="label %s">%s</span>' % (label_class, label_content)

    @classmethod
    def table_row(cls, row_contents):
        return '<tr>%s</tr>' % row_contents

    @classmethod
    def styles(cls):
        """
        Returns a stylesheet for the HTML Report.
        @rtype: str
        """
        return '''
        <style>
            .key_icon {text-align: center}
            .icon-gray {color: #808080}
        </style>
        '''


#==============================================================================
# HTML Message Styles definitions
#==============================================================================

class _HTMLMessageStyle(object):
    bootstrap_icon_class = ""
    bootstrap_row_class = ""
    bootstrap_attention_label_class = "label-danger"


class _HTMLInfoMessageStyle(_HTMLMessageStyle):
    bootstrap_icon_class = "glyphicon-info-sign icon-gray"


class _HTMLErrorMessageStyle(_HTMLMessageStyle):
    bootstrap_icon_class = "glyphicon-minus-sign icon-gray"
    bootstrap_row_class = "danger"


class _HTMLWarningMessageStyle(_HTMLMessageStyle):
    bootstrap_icon_class = "glyphicon-warning-sign icon-gray"
    bootstrap_row_class = "warning"


#==============================================================================
# HTML Report elements definitions
#==============================================================================

class HTMLReportColumn(object):

    def __init__(self, key, title):
        self.key = key
        self.title = title

    def __str__(self):
        return "%s" % self.key

    def __repr__(self):
        return "<column key '%s'>" % self.key

    def format_html_table_cell(self):
        return '<th class="header_%s">%s</th>' % (self.key, self.title)


class HTMLReportColumns(list):

    def __init__(self, columns):
        assert type(columns) is list

        super(self.__class__, self).__init__()

        for column in columns:
            self.append(HTMLReportColumn(column[0], column[1]))


class HTMLMessage(object):

    style_class = None
    attention_label = "Attention"

    def __init__(self, content, keys_content={}, attention_key=None):
        if attention_key is not None:
            assert keys_content != {}

        self.parent_report = None
        self.keys_content = deepcopy(keys_content)
        self.keys_content['content'] = content
        self.attention_key = attention_key

    def __repr__(self):
        return str(self.keys_content)

    def icon_cell_content(self):
        """
        Returns the contents for the cells that contain the icon for the message.
        @rtype : str
        """
        return _HTMLReportTemplate.bootstrap_icon(self.style_class.bootstrap_icon_class)

    def attention_cell_content(self, content):
        return "%s %s" % (content, _HTMLReportTemplate.bootstrap_label(self.style_class.bootstrap_attention_label_class, self.attention_label))

    def format_html_table_row(self, parent_report, id=-1):
        row_template = "<tr class=\"%s\">%s</tr>\n"
        cell_template = "<td class=\"%s\">%s</td>"
        row_content = ""

        for column in parent_report.columns:
            if column.key == HTMLReport.COLUMN_ICON_KEY:
                cell_value = self.icon_cell_content()
            elif column.key == HTMLReport.COLUMN_ID_KEY:
                cell_value = str(id) if id > -1 else ""
            else:
                cell_value = self.value_for_key(column.key)
                cell_value = parent_report.default_value_for_key(column.key) if cell_value is None else cell_value
                if self.attention_key == column.key:
                    cell_value = self.attention_cell_content(cell_value)
            row_content += cell_template % (self.__html_cell_classes(column), cell_value)

        return row_template % (self.__html_row_classes(), row_content)

    def value_for_key(self, key):
        if key in self.keys_content:
            return self.keys_content[key]
        else:
            return None

    def set_value_for_key(self, key, value):
        self.keys_content[key] = value

    def __html_row_classes(self):
        return self.style_class.bootstrap_row_class

    def __html_cell_classes(self, column):
        return "key_%s" % column.key


class InfoMessage(HTMLMessage):

    style_class = _HTMLInfoMessageStyle


class ErrorMessage(HTMLMessage):

    style_class = _HTMLErrorMessageStyle
    attention_label = "Error"


class WarningMessage(HTMLMessage):

    style_class = _HTMLWarningMessageStyle


#==============================================================================
# HTML Report definition
#==============================================================================

class HTMLReport(object):

    COLUMN_ID_KEY = "id"
    COLUMN_ID_TITLE = "#"
    COLUMN_ICON_KEY = "icon"
    COLUMN_ICON_TITLE = ""
    COLUMN_CONTENT_KEY = "content"
    COLUMN_CONTENT_TITLE = "Message"

    def __init__(self, columns, title="Report", show_icons=True):

        default_columns = [
            HTMLReportColumn(HTMLReport.COLUMN_ID_KEY, HTMLReport.COLUMN_ID_TITLE),
            HTMLReportColumn(HTMLReport.COLUMN_CONTENT_KEY, HTMLReport.COLUMN_CONTENT_TITLE),
        ]

        for column in columns:
            if column.key in [HTMLReport.COLUMN_ID_KEY, HTMLReport.COLUMN_ICON_KEY]:
                raise Exception("Column keys %s are reserved for internal use by HTMLReport class."
                                % str(default_columns))

        self.columns = default_columns
        if show_icons:
            self.columns.insert(0, HTMLReportColumn(HTMLReport.COLUMN_ICON_KEY, HTMLReport.COLUMN_ICON_TITLE), )
        self.columns.extend(columns)
        self.messages = []
        self.default_column_values = {}
        self.__show_icons = show_icons
        self.title = title
        self.header_message = None
        self.footer_message = None

    def number_of_columns(self):
        return len(self.columns)

    def column_index_for_key(self, key):
        for i in range(0, len(self.columns)):
            if self.columns[i].key == key:
                return i
        return INDEX_NOT_FOUND

    def append_message(self, message):
        """
        Appends a new message to the report.
        @type self: object
        @type message: HTMLMessage
        """
        self.messages.append(message)

    def append_messages(self, messages):
        """
        Extends the list of messages with the list of new messages.
        @type messages: list
        """
        assert type(messages) is list
        self.messages.extend(messages)

    def messages_count(self, message_type=None):
        if message_type is None:
            return len(self.messages)

        counter = 0
        for message in self.messages:
            if type(message) is message_type:
                counter += 1

        return counter

    def format_html(self):

        html_table_head = self.format_html_table_head()
        html_table_body = self.format_html_table_body()
        html_header = _HTMLReportTemplate.report_heading(self.title,
                                                         self.messages_count(ErrorMessage),
                                                         self.messages_count(WarningMessage)
        )
        html_footer = ""

        if self.header_message is not None:
            html_header += _HTMLReportTemplate.bootstrap_info_alert(self.header_message)

        if self.footer_message is not None:
            html_footer += _HTMLReportTemplate.bootstrap_info_alert(self.footer_message)

        html = _HTMLReportTemplate.html(
            _HTMLReportTemplate.styles() + _HTMLReportTemplate.bootstrap_stylesheet(),
             html_header + _HTMLReportTemplate.table(html_table_head, html_table_body) + html_footer
        )

        return html

    def format_html_table_head(self):
        header_contents = ""
        for column in self.columns:
            header_contents += column.format_html_table_cell()

        return _HTMLReportTemplate.table_row(header_contents)


    def format_html_table_body(self):
        html = ""
        id = 0
        for message in self.messages:
            id += 1
            html += message.format_html_table_row(self, id)


        return html


    @classmethod
    def default_cell_value(cls):
        return "(empty)"

    def default_value_for_key(self, key):
        if key in self.default_column_values:
            return self.default_column_values[key]
        else:
            return self.default_cell_value()


#==============================================================================
# Unit tests
#==============================================================================

class _HTMLReportTests(unittest.TestCase):

    def testReport(self):
        report = HTMLReport([HTMLReportColumn('file_name', 'File Name')], show_icons=False, title="Test Report")
        for i in range(1,10):
            report.append_message(ErrorMessage('A horrible error %d' % i))
        for i in range(1,10):
            report.append_message(WarningMessage('Just a warning %d' % i))

        report.header_message = "This is some header message."
        report.footer_message = "This is just a footer message. Nothing special."
        html_contents = report.format_html()

        re_result = re.findall("A horrible error \d+", html_contents)
        self.assertEqual(9, len(re_result))
        re_result = re.findall("Just a warning \d+", html_contents)
        self.assertEqual(9, len(re_result))
        re_result = re.findall("This is some header message\.", html_contents)
        self.assertEqual(1, len(re_result))
        re_result = re.findall("This is just a footer message. Nothing special\.", html_contents)
        self.assertEqual(1, len(re_result))

    def testReportColumns(self):

        report = HTMLReport(HTMLReportColumns([
            ('column1', 'Column 1'),
            ('column2', 'Column 2'),
            ('column3', 'Column 3')
        ]))

        self.assertEqual('column1', report.columns[3].key)
        self.assertEqual('column2', report.columns[4].key)
        self.assertEqual('column3', report.columns[5].key)
        self.assertEqual('Column 1', report.columns[3].title)
        self.assertEqual('Column 2', report.columns[4].title)
        self.assertEqual('Column 3', report.columns[5].title)




