from distutils.core import setup

setup(
    name='htmlreport',
    version='0.1',
    py_modules=['htmlreport'],
    url='https://bitbucket.org/pixellu-open-source/python-htmlreport',
    license='GPLv3',
    author='Alex Prykhodko (Pixellu LLC)',
    author_email='alex@prykhodko.net',
    description=''
)
